$.get("/static/tutorial/tutorial_1/tutorial_1_0.txt", function(txt) {
	$("#tut_1_text").html(txt);
});

var tsumego_id = document.getElementById("tsumego_id");
var tsumego = new WGo.Tsumego(tsumego_id, {
	sgfFile: "/static/tutorial/tutorial_1/tutorial_1_0.sgf",
	displayHintButton: false,
	//debug: true, /* remove this line hide solution */
});
tsumego.setCoordinates(true);

var section = 0;
var LAST_SECTION = 5;

var checkTimer = setInterval(checkComment,1000);
function checkComment(){
	var text = $('.wgo-tsumego-comment').text();
	if (checkContinue(text)){
		clearInterval(checkTimer);
		if (section === LAST_SECTION){
			$("#tut_1_finish").toggle();
		}
		else{
			$("#tut_1_button").toggle();
		}
	}
}

$("#tut_1_button").click(function(){
	section++;
	$.get("/static/tutorial/tutorial_1/tutorial_1_"+section+".sgf", function(sgf) {
		var kifu = WGo.SGF.parse(sgf);
		tsumego.loadKifu(kifu);
	});
	$.get("/static/tutorial/tutorial_1/tutorial_1_"+section+".txt", function(txt) {
		$("#tut_1_text").html(txt);
	});
	$("#tut_1_button").toggle();
	checkTimer = setInterval(checkComment,1000);
});

$("#tut_1_finish").click(function(){
	function getCookie(name) {
		var cookieValue = null;
		if (document.cookie && document.cookie != '') {
			var cookies = document.cookie.split(';');
			for (var i = 0; i < cookies.length; i++) {
				var cookie = jQuery.trim(cookies[i]);
				// Does this cookie string begin with the name we want?
				if (cookie.substring(0, name.length + 1) == (name + '=')) {
					cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
					break;
				}
			}
		}
		return cookieValue;
	}
	var csrftoken = getCookie('csrftoken');

	var postdata = {
		'id': 1,
		'csrfmiddlewaretoken': csrftoken
	};

	// function postJson(url, jsObj, whenSuccess, whenError){
  //   $.ajax({
  //       type: "post",
  //       contentType: "application/json; charset=utf-8",
  //       accepts: {
  //           xml: 'text/xml',
  //           text: 'text/plain'
  //       },
  //       dataType: "json",
  //       url: url,
  //       data: JSON.stringify(jsObj),
  //       success: function(result){
  //           if(whenSuccess !== undefined){ whenSuccess(result); }
  //       },
  //       error: function(xhr){
  //           if(whenError !== undefined){ whenError(xhr.status); }
  //       }
  //   });
	// }
	// postJson('/journey/',postdata,function(){
	// 	console.log("success");
	// 	window.location = '/journey';
	// }, function(){
	// 	console.log("failure");
	// 	window.location = '/journey';
	// })
	$.post('/journey/', postdata).done(function(data){
		window.location = '/journey';
	});
});
