By sending support, the soldier went from having one liberty to three! But
sometimes, some soldiers are done for, and we can't save them sadly.
<br>
In the situation shown, we see that even if we try to send support to it, not
only will the original soldier die, the support will also die as well!
<br>
<br>
Just for demonstration, attempt to add support at the X and watch them both die! This is
only a simulation, so don't feel too bad.
