"""
File for queries.py test
"""

from django.contrib.auth.models import User
from django.db.models.manager import Manager
from django.test import TestCase
from core.models import GoUser, JourneyNodes
from core import queries
from core.queries import get_level, update_go_user
from unittest.mock import Mock, patch

class QueriesTest(TestCase):
    """
    Queries Test Class
    """
    def setUp(self):
        """
        Create model objects
        """
        username = 'testuser'
        password = 'testpass'
        self.user = User.objects.create_user(username, password=password)
        self.client.login(username=username, password=password)
        self.node = JourneyNodes.objects.create(name='tnode', region='tregion', link='tlink')
        self.node2 = JourneyNodes.objects.create(name='tnode', region='tregion', link='tlink')

    def test_get_level(self):
        """
        Test get_level returninga valid pk
        """
        self.gouser = GoUser.objects.create(user_id=self.user, last_completed_node=self.node)
        self.gouser.save()
        self.assertTrue(get_level(self.user), self.node.pk)
        
    @patch.object(queries,'get_level')
    def test_mock_get_level_try(self, mock_get_level):
        """
        Test mock get level to return a number
        """
        mock_get_level.return_value = 1
        self.assertEqual(queries.get_level(self.user, Mock()), 1)

    def test_get_level_exception(self):
        """
        Test 0 is returned with exception raised
        """
        with patch.object(Manager, 'get') as mock_get:
            mock_get.side_effect = GoUser.DoesNotExist
        self.assertEqual(get_level(self.user), 0)

    @patch('core.queries.logging')
    def test_and_log_update_go_user_returns_false(self, mock_log):
        """
        Test for both instances when update_go_user returns false
        """
        self.assertFalse(update_go_user(self.user, 100))
        self.assertTrue(mock_log.error.called)
        self.gouser = GoUser.objects.create(user_id=self.user, last_completed_node=self.node)
        self.gouser.save()
        self.assertFalse(update_go_user(self.user, 100))
        self.assertTrue(mock_log.error.called)

    def test_update_go_user_returns_true(self):
        """
        Test for both instances when update_go_user returns false
        """
        self.assertTrue(update_go_user(self.user,self.node2.pk))
        self.assertTrue(update_go_user(self.user,self.node2.pk))

    @patch.object(queries,'update_go_user')
    def test_mock_update_go_user_is_false(self, mock_update_go_user):
        """
        Test mock update_go_user returning false
        """
        mock_update_go_user.return_value = False
        self.assertEqual(queries.update_go_user(self.user, Mock()), False)

    @patch.object(queries,'update_go_user')
    def test_mock_update_go_user_is_true(self, mock_update_go_user):
        """
        Test mock update_go_user returning true
        """
        mock_update_go_user.return_value = True
        self.assertEqual(queries.update_go_user(self.user, Mock()), True)
