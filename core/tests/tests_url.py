"""
File for URL test
"""
from django.test import TestCase
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from core.models import JourneyNodes, GoUser

class URLTest(TestCase):
    """
    URL Test class
    """
    def test_journey(self):
        """
        Test journey URL
        """
        response = self.client.get(reverse('journey'))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "Journey Map")

    def test_settings(self):
        """
        Test settings URL
        """
        response = self.client.get(reverse('settings'))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "Settings")

    def test_profile(self):
        """
        Test profile URL
        """
        username = 'testuser'
        password = 'testpass'
        self.user = User.objects.create_user(username, password=password)
        self.client.login(username=username, password=password)
        response = self.client.get(reverse('profile'))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "Profile")

    def test_visits(self):
        """
        Test visit URL
        """
        response = self.client.get(reverse('visits'))
        self.assertEqual(response.status_code, 200)

    def test_tutorial(self):
        """
        Test tutorial URLs
        """
        for num in range(1,7):
            response = self.client.get(reverse('tutorial', args=[num]))
            self.assertEqual(response.status_code, 200)
