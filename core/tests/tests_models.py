"""
File for Model tests
"""
from django.test import TestCase
from django.contrib.auth.models import User
from core.models import JourneyNodes, GoUser, UserCompletedNodes

class ModelsTest(TestCase):
    """
    Models test class
    """
    def setUp(self):
        """
        Create test user
        """
        self.user = User.objects.create_user('testuser', password='testpass')

        self.node = JourneyNodes.objects.create(name='tnode', region='tregion', link='tlink')

    def test_journey_node_creation(self):
        """
        Test creation of JourneyNode object
        """
        self.assertTrue(isinstance(self.node, JourneyNodes))
        self.assertEqual(self.node.__str__(), 'tnode tregion tlink')

    def test_go_user_creation(self):
        """
        Test creation of GoUser object
        """
        gouser = GoUser.objects.create(user_id=self.user, last_completed_node=self.node)
        self.assertTrue(isinstance(gouser, GoUser))
        self.assertEqual(gouser.__str__(), 'testuser tnode tregion tlink')

    def test_user_nodes_creation(self):
        """
        Test creation of UserCompletedNodes object
        """
        user2 = User.objects.create_user('testuser2', password='testpass2')
        user_node = UserCompletedNodes.objects.create(user_id=self.user, node_id=self.node)
        user2_node = UserCompletedNodes.objects.create(user_id=user2, node_id=self.node)
        self.assertTrue(isinstance(user_node, UserCompletedNodes))
        self.assertTrue(isinstance(user2_node, UserCompletedNodes))
        self.assertEqual(user_node.__str__(), 'testuser tnode tregion tlink')
        self.assertEqual(user2_node.__str__(), 'testuser2 tnode tregion tlink')
