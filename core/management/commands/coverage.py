"""
Adds the coverage command to manage.py
Runs a test coverage on the core app

Invoke by `python manage.py coverage`
"""
from subprocess import call

from django.core.management.base import BaseCommand

class Command(BaseCommand):
    """
    Required class to work with manage.py
    """
    help = 'Runs coverage and outputs statistics'

    def handle(self, *args, **option):
        #pylint: disable=unused-argument
        cmd = ("coverage run --source='.' --omit='venv/*,core/management/*,"
               "core/tests/*,core/migrations/*,goto_go_trainer/settings.py,"
               "goto_go_trainer/dev_settings.py,goto_go_trainer/wsgi.py' "
               "manage.py test core/tests; coverage report;")
        call(cmd, shell=True)
