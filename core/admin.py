"""
Admin file that allows us to register our models
Allows us to manage them in the admin interface
"""

from django.contrib import admin

from . import models

# Register your models here.
admin.site.register(models.GoUser)
admin.site.register(models.JourneyNodes)
admin.site.register(models.UserCompletedNodes)
