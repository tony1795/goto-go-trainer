"""
Template tags for handling JourneyNodes objects
"""
from django import template
from core.models import JourneyNodes

register = template.Library()

@register.assignment_tag
def get_regions():
    """
    Return a list of all regions
    """
    all_regions = JourneyNodes.objects.values_list('region', flat=True)
    regions = []
    for region in all_regions:
        if region not in regions:
            regions.append(region)
    return regions
