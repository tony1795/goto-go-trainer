# Goto Go Trainer

A website to help guide players of all levels in Go to get stronger!

## Running Locally

Make sure Git and Python 3 is installed!

```sh
$ git clone git@bitbucket.org:goto_go_trainer/goto-go-trainer.git

$ cd goto-go-trainer
```

Now, there is one external library we need to clone as well once inside.

```sh
$ git clone https://github.com/waltheri/wgo.js.git
```

Next, we create our Django project's database by opening a shell session for the postgres user. 
Log into a Postgres session.
```sh
$ psql
```

In the shell, we create a database called database goto_go_trainer.
```sh
postgres=# CREATE DATABASE goto_go_trainer;
```

Next, we create a database user which we will use to connect to and interact with the database. 
```sh
postgres=# CREATE USER goto_go_trainer WITH PASSWORD '_w3y3n0tg0+';
```

Afterwards, we modify a few of the connection parameters for the user we just created. This will speed up database operations so that the correct values do not have to be queried and set each time a connection is established.
We are setting the default encoding to UTF-8, which Django expects. We are also setting the default transaction isolation scheme to "read committed", which blocks reads from uncommitted transactions. Lastly, we are setting the timezone. By default, our Django projects will be set to use UTC:
```sh
postgres=# ALTER ROLE goto_go_trainer SET client_encoding TO 'utf8';
postgres=# ALTER ROLE goto_go_trainer SET default_transaction_isolation TO 'read committed';
postgres=# ALTER ROLE goto_go_trainer SET timezone TO 'UTC';
```

Now, all we need to do is give our database user access rights to the database we created.
```sh
postgres=# GRANT ALL PRIVILEGES ON DATABASE goto_go_trainer TO goto_go_trainer;
```

Exit the SQL prompt to get back to the postgres user's shell session.
```sh
postgres=# \q
```

Exit out of the postgres user's shell session to get back to your regular user's shell session.
```sh
postgres=# exit
```

Then, we connect the database to our project. Create a dev_settings.py file and place below in the same level as settings.py in the goto_go_trainer directory. Add the following lines of code in dev_settings.py:

```sh
DATABASE_URL = "postgres://goto_go_trainer:_w3y3n0tg0+@localhost/goto_go_trainer"
SECRET_KEY = ''
DEBUG = True
DATABASE_NAME = 'goto_go_trainer'
DATABASE_USER = 'goto_go_trainer'
DATABASE_PASSWORD = '_w3y3n0tg0+'
DATABASE_HOST = 'localhost'
```

Notice that SECRET_KEY variable is empty, it is recommended that you fill it with a secret key that is only 
known to you. 

At this point, it is recommended you create a virtual environment for the pip installation.

```sh
$ pyvenv venv

$ source venv/bin/activate
(venv) $
```
Once you are in your virtual environment, run the following:

```sh
(venv) $ pip install -r requirements.txt

(venv) $ python manage.py migrate
(venv) $ python manage.py collectstatic

(venv) $ python manage.py runserver
```

Your app should now be running on [localhost:8000](http://localhost:8000/).

To deactivate the virtual environment:
```sh
(venv) $ deactivate
$
```

For coverage and linting of Python code, run the following:
```sh
(venv) $ python manage.py coverage
(venv) $ python manage.py lint
```

For Javascript testing, Node and NPM must be installed.

While in the root directory, run:
```sh
$ npm install
$ npm test
```

For Selenium Testing, chromedriver needs to be installed from:

https://chromedriver.storage.googleapis.com/index.html?path=2.25/

Download the one that corresponds with your OS and add it to
your PATH